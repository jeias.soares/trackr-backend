var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var userSchema = Schema({
    id: { type: String, index: true, required: true, unique: true },
	name: { type: String, required: true },
	email : { type: String },
	first_name: { type: String, required: false },
	last_name: { type: String, required: false },
	picture: {
		data: {
		  is_silhouette: { type: Boolean, required: false },
		  url: String
		}
	},
    location: {
    	id: { type: String, required: false },
    	name: { type: String, required: false },
    	latitude: { type: String, required: false },
    	longitude: { type: String, required: false },
    	lastUpdated: { type: String, required: false }
    },
    friends: {
    	data: [{
          id: String,
          name: String,
        	first_name: { type: String, required: false },
        	last_name: { type: String, required: false },
          picture: {
          	data: {
          	  url: String
          	}
          },
          location: {
          	id: { type: String, required: false },
          	name: { type: String, required: false }
          }
    	}]
    }
}, {
	timestamps: true
});

userSchema.post('init', function(user) {
  console.log('%s has been initialized from the db', user._id);
});

userSchema.post('validate', function(user) {
  console.log('%s has been validated (but not saved yet)', user._id);
});

userSchema.post('save', function(user) {
  console.log('%s has been saved', user._id);
});

userSchema.post('remove', function(user) {
  console.log('%s has been removed', user._id);
});

module.exports = mongoose.model('User', userSchema);