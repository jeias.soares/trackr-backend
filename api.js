// Pegando uma instância do Express Router para criar rotas da nossa API
var express = require('express');
var router = express.Router();

// Models
var User = require('./models/user');

// Middleware específico para esse roteador, que será utilizado para todas as requisições na nossa API
router.use(function (request, response, next) {
  next();
});

// Todas as requisições que baterem sem nenhum parâmetro serão redirecionadas para essa rota
router.get('/', function(request, response) {
  response.json({ success: true, code: 0, message: 'Nada aqui', data: {} });
});

router.route('/users')
  .get(function (request, response) {

		User.find(function(error, users) {
			if (error) {
			  return response.json({ success: false, code: '', message: 'Não foi possível retornar os usuários', data: {} });
			}

			response.json({ success: true, code: 0, message: '', data: users });
	  });
  })
  .post(function (request, response) {

    var user = new User(request.body);

    user.save(function(error) {

      if (error) {
        return response.json({ success: false, code: error.code, message: error.errmsg, data: {} });
      }

      response.json({ success: true, code: 0, message: 'Usuário criado!', data: user });
    });
  });

router.route('/users/:id')
  .get(function(request, response) {
      User.findOne({ id: request.params.id }, function(error, user) {
        if (error) {
          return response.json({ success: false, code: error.code, message: error.errmsg, data: {} });
        }
        
        if (user === null) {
          return response.json({ success: false, code: "", message: "Usuário não encontrado", data: {} });
        }

        response.json({ success: true, code: 0, message: '', data: user });
      });
  })
  .post(function (request, response) {
      response.json({ success: false, code: 1, message: 'Erro! Use PUT para atualizar dados', data: {} });
    })
  .put(function(request, response) {
      User.findOne({ id: request.params.id }, function(error, user) {
          if (error) {
            return response.json({ success: false, code: error.code, message: error.errmsg, data: {} });
          }
          
          if (user === null) {
            return response.json({ success: false, code: "", message: "Usuário não encontrado", data: {} });
          }
          
          delete request.body['__v'];
          delete request.body['updatedAt'];
          delete request.body['createdAt'];
          delete request.body['_id'];
          
          for (var key in request.body) {
            if (request.body.hasOwnProperty(key)) {
              user[key] = request.body[key];
            }
          }

          user.save(function(error) {
              if (error) {
                return response.json({ success: false, code: error.code, message: error.errmsg, data: {} });
              }

              response.json({ success: true, code: 0, message:'O usuário ' + user.id + 'foi atualizado !!', data: user });	
          });
      });
  })
  .delete(function(request, response) {
    User.remove({
      id: request.params.id
    }, function(error, user) {
        if (error) {
          return response.json({ success: false, code: error.code, message: error.errmsg, data: {} });
        }
        
        if (user === null) {
          return response.json({ success: false, code: "", message: "Usuário não encontrado", data: {} });
        }

        response.json({ success: true, code: 0, message: 'O usuário ' + user.id + ' foi deletado!', data: user });
    });
  });
  
router.route('/users/:id/:property')
  .get(function(request, response) {
      User.findOne({ id: request.params.id }, function(error, user) {
        if (error) {
          return response.json({ success: false, code: error.code, message: error.errmsg, data: {} });
        }
        
        if (user === null) {
          return response.json({ success: false, code: "", message: "Usuário não encontrado", data: {} });
        }
        
        if (typeof user[request.params.property] === 'undefined') {
          return response.json({ success: false, code: "", message: "Propriedade não encontrada", data: {} });
        }

        response.json({ success: true, code: 0, message: '', data: user[request.params.property] });
      });
  });

module.exports = router;