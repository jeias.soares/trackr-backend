# Instalação

Instale as dependências do projeto:

```shell
npm install
```

Crie o banco de dados:
```shell
mongo
use frinder
```

# Executando

```shell
# Iniciar o MongoDB
chmod a+x mongod.sh
./mongod.sh
```

Se der erro "Unclean shutdown detected.", execute o script com o argumento **--repair**.

Clique em "> Run" (ALT-F5)

# Usando

## CREATE

Para salvar um novo usuário, envie um request do tipo POST para **https://frinder-vieiram.c9users.io/api/users/** com Headers:

```
Content-Type:application/json
```

e Body:


```json
{
  "id": "1064592026919758",
  "name": "Vandré Leal Cândido",
  "first_name": "Vandré",
  "picture": {
    "data": {
      "url": "https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/10151865_807050712673892_6208678016368115756_n.jpg?oh=481e69c50fb3ac2029799c5e585a247d&oe=57E26234"
    }
  },
  "location": {
    "id": "104099496292845",
    "name": "Uberlândia"
  },
  "friends": {
    "data": [
      {
        "id": "1",
        "name": "Rafael Vieira Mendes",
        "picture": {
          "data": {
            "url": "https://scontent.xx.fbcdn.net/v/t1.0-1/c17.17.216.216/s50x50/1604715_705034676194625_827570756_n.jpg?oh=e82b2be1e3df89eb0a6b2b6137083014&oe=57DC9ACC"
          }
        },
        "location": {
          "id": "108653189159915",
          "name": "Araguari"
        }
      },
      {
        "id": "2",
        "name": "Vitor Oliveira",
        "picture": {
          "data": {
            "url": "https://scontent.fudi1-1.fna.fbcdn.net/hprofile-xtp1/v/t1.0-1/p160x160/12193274_1053309211346262_3551305679837709877_n.jpg?oh=b6b06ef13f6d87929a32f9a6c1713be2&oe=5791FD03"
          }
        },
        "location": {
          "id": "104099496292845",
          "name": "Uberlândia"
        }
      },
      {
        "id": "3",
        "name": "Jeías Soares",
        "picture": {
          "data": {
            "url": "https://scontent.fudi1-1.fna.fbcdn.net/hprofile-ash2/v/t1.0-1/c1.0.160.160/p160x160/484835_545934822170404_2067002361_n.jpg?oh=eadc69d980db6527dd38e1c6c195bc9f&oe=57886278"
          }
        },
        "location": {
          "id": "104099496292845",
          "name": "-18.933451,-48.27537640000003"
        }
      },
      {
        "id": "4",
        "name": "Gilson Mendes",
        "picture": {
          "data": {
            "url": "https://scontent.fudi1-1.fna.fbcdn.net/hprofile-xlt1/v/t1.0-1/p160x160/11817206_870976202983328_6173251577235295341_n.jpg?oh=109a72f98919814c7a0b44c1941cb1f4&oe=5786A89F"
          }
        },
        "location": {
          "id": "104099496292845",
          "name": "-18.9184584,-48.258949099999995"
        }
      },
      {
        "id": "5",
        "name": "José Junio",
        "picture": {
          "data": {
            "url": "https://scontent.fudi1-1.fna.fbcdn.net/v/t1.0-1/p160x160/12814751_1057094607687581_4209967882919805346_n.jpg?oh=2414140d6984eb6f4888da0b91718e19&oe=57E4F443"
          }
        },
        "location": {
          "id": "104099496292845",
          "name": "-18.9082399,-48.26136070000001"
        }
      }
    ],
    "paging": {
      "cursors": {
        "before": "QVFIUldaUXB4NDBJM0JUc295UF85dnVEXzRfVUZAqVm5GMzN2cnRTaWNiQlNleU9MWmt3SnhPWjJCeW9aa010ZA3pIU1dVeGFuMEFZASXBhdVcxYmRDME5rUjNn",
        "after": "QVFIUm5sNUxQRU83Mkx3bnVHbmxEeFpSMXdrRVlyYTVEcFlKXzg3cjJuam45dGdmM1RrSnVLaGZAFazM1TTJsdUdYZAlNranNtSVZAqamRmUmRUWFU0MmRFR3ZAn"
      }
    },
    "summary": {
      "total_count": 346
    }
  }
}
```

Exemplo de retorno:

```json
```

## READ

Para obter dados de todos os usuários, envie um request do tipo GET para **https://frinder-vieiram.c9users.io/api/users/**. Exemplo de retorno:

Exemplo de retorno:

```json
```

Para obter dados de todos os usuários, envie um request do tipo GET para **https://frinder-vieiram.c9users.io/api/users/:id** substituindo **:id** pelo ID do usuário desejado. Exemplo de retorno:

Exemplo de retorno:

```json
```

## UPDATE

Para atualizar um usuário existe, envie um request do tipo PUT para
