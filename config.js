var config = {};

// Definindo a porta da nossa aplicação
config.port = process.env.PORT || 5500;

config.base_url = 'https://' + process.env.IP + ':' + config.port;

// MongoDB URL
config.db_string= 'mongodb://' + process.env.IP + ':27017/frinder';

module.exports = config;
