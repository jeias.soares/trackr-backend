var config = require('./config');

// Invocando os pacotes necessários
var express = require('express');
var parser = require('body-parser');
var cors = require('cors');

// Definindo que nossa aplicação irá utilizar o Express
var app = express();

// Habilitar requests CORS
app.use(cors());

// Configurando a aplicação para utilizar body-parser para conseguir dados vindos do POST
app.use(parser.urlencoded({ extended: true }));
app.use(parser.json());

// Conectando ao banco Mongo
var mongoose = require('mongoose').connect(config.db_string);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'Erro ao conectar ao banco'));

// Tornar banco acessível a partir dos roteadores
app.use(function(request, response, next) {
    request.db = db;
    next();
});

// Todas nossas rotas terão o prefixo /api
var api = require('./api');
app.use('/api', api);

// Escutando a porta definida
app.listen(config.port);
console.log('Waiting on port ' + config.port);